public class Topic4 {
    public static void main(String[] args){
        //2 4 8  16 32
        int initial = 2;
        for (int i = 1;i<=5;i++){
            System.out.print(((int)Math.pow(initial,i)) + " ");
        }

        // 2    4   8   32  256
        int a = 2;
        int b = 4;
        int c;
        System.out.println(" ");
        System.out.print(a + " " + b);
        for (int i = 0; i<3;i++){
            c=a*b;
            System.out.print(" " + c);
            a=b;
            b=c;
        }

        // 6    6   5   9   2
        int[] array = {56,48,55,57,12,1,0,46,555,31,13};
        System.out.println(" ");
        int kosong;
        for (int j = 0;j<array.length;j++){
            for (int i =0;i<array.length;i++){
                if (i<array.length-1){
                    if(array[i]>array[i+1]){
                        kosong=array[i+1];
                        array[i+1]=array[i];
                        array[i]=kosong;
                    }
                }
            }
        }
        for(int element:array){
            System.out.print(element+" ");
        }

        //  M   A   K   A   N   N   A   S   I
        char[] charArray = {'M','A','K','A','N','N','A','S','I'};
        System.out.println(" ");
        char kosong2;
        for (int j = 0;j<charArray.length;j++){
            for (int i =0;i<charArray.length;i++){
                if (i<charArray.length-1){
                    if((int)charArray[i]>(int)charArray[i+1]){
                        kosong2=charArray[i+1];
                        charArray[i+1]=charArray[i];
                        charArray[i]=kosong2;
                    }
                }
            }
        }
        for(char element:charArray){
            System.out.print(element+" ");
        }
    }
}
